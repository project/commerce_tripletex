<?php

/**
 * Implements hook_drush_command().
 */
function commerce_tripletex_drush_command() {
  $items['commerce-tripletex-sync-products'] = array(
    'description' => "Syncs all drupal products with data from Tripletex.",
  );
  return $items;
}

/**
 * Sync all Drupal products with Tripletex.
 */
function drush_commerce_tripletex_sync_products() {
  $user = tripletex_api_get_user();
  $app = tripletex_api_get_app($user);

  // Get products.
  /** @var \zaporylie\Tripletex\Model\Product\Product[] $products */
  $products = $app->product()->getList()->getValues();
  $skus = array_map(function (\zaporylie\Tripletex\Model\Product\Product $product) { return $product->getNumber();}, $products);

  // Get matching commerce products.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_product');
  $result = $query->execute();

  foreach (array_keys($result['commerce_product']) as $product_id) {
    try {
      _commmerce_tripletex_drush_map_product_by_id($product_id, $app, $products);
      watchdog('commerce_tripletex', 'Product @product_id synced', array('@product_id' => $product_id), WATCHDOG_NOTICE);
    }
    catch (Exception $exception) {
      watchdog('commerce_tripletex', 'Unable to sync product @product_id: %message', array('@product_id' => $product_id, '%message' => $exception->getMessage()), WATCHDOG_ERROR);
    }
  }
}

/**
 * @param $product_id
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param \zaporylie\Tripletex\Model\Product\Product[] $products
 *
 * @throws \Exception
 */
function _commmerce_tripletex_drush_map_product_by_id($product_id, \zaporylie\Tripletex\Tripletex $app, array $products) {
  if (!$product = commerce_product_load($product_id)) {
    throw new Exception('Unable to load product');
  }

  // If product is missing remote id, find one based on sku.
  if (!$product->data['commerce_tripletex']['remote_id']) {
    $product->data['commerce_tripletex']['remote_id'] = _commerce_tripletex_match_product_id($product->sku, $products);
  }

  // No products were found.
  if (empty($product->data['commerce_tripletex']['remote_id'])) {
    throw new Exception('Unable to find matching tripletex product');
  }

  // Load helper methods.
  module_load_include('inc', 'commerce_tripletex');

  // Modify product.
  commerce_tripletex_sync_product($app, $product);

  // Save product.
  commerce_product_save($product);
}

/**
 * @param $sku
 * @param \zaporylie\Tripletex\Model\Product\Product[] $products
 */
function _commerce_tripletex_match_product_id($sku, array $products) {
  foreach ($products as $product) {
    if ($product->getNumber() === $sku) {
      return $product->getNumber();
    }
  }
}
