<?php

/**
 * @file
 * Helper methods to be used against Tripletex.
 */

/**
 * Creates (or updates) order in Tripletex.
 *
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param object $order
 * @param array $context
 *
 * @return \zaporylie\Tripletex\Model\Order\Order
 */
function commerce_tripletex_sync_order(\zaporylie\Tripletex\Tripletex $app, $order, array $context = array()) {
  $tripletex_order = new \zaporylie\Tripletex\Model\Order\Order();
  $tripletex_order
    ->setOrderDate((new DateTime())->setTimestamp($order->created))
    ->setDeliveryDate((new DateTime())->setTimestamp($order->changed))
    ->setReceiverEmail($order->mail)
    ->setReference($order->order_id)
    ->setIsPrioritizeAmountsIncludingVat(FALSE);

  if (isset($context['tripletex_customer']) && $context['tripletex_customer'] instanceof \zaporylie\Tripletex\Model\Customer\Customer) {
    /** @var \zaporylie\Tripletex\Model\Customer\Customer $tripletex_customer */
    $tripletex_customer = $context['tripletex_customer'];
    $tripletex_order
      ->setCustomer($tripletex_customer)
      ->setDeliveryAddress($tripletex_customer->getDeliveryAddress());
  }

  // If Order was already created in Tripletex, use reeference.
  if (!empty($order->data['commerce_tripletex']['remote_id'])) {
    try {
      $app->order()->getOrder($order->data['commerce_tripletex']['remote_id']);
      // If no exception thrown, use remote id.
      $tripletex_order->setId($order->data['commerce_tripletex']['remote_id']);
    }
    catch (Exception $exception) {
      // Mute.
    }
  }

  // Allow to customize Order before sending it to Tripletex.
  drupal_alter('commerce_tripletex_sync_order', $app, $tripletex_order, $order, $context);

  // Create or update Order in Tripletex.
  if ($tripletex_order->getId()) {
    $tripletex_order = $app->order()->updateOrder($tripletex_order)->getValue();
  }
  else {
    $tripletex_order = $app->order()->createOrder($tripletex_order)->getValue();
    $order->data['commerce_tripletex']['remote_id'] = $tripletex_order->getId();

  }
  return $tripletex_order;
}

/**
 * Creates new order line in Tripletex.
 *
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param object $line_item
 * @param array $context
 * @param bool $send
 *
 * @return \zaporylie\Tripletex\Model\Order\OrderLine
 */
function commerce_tripletex_sync_order_line(\zaporylie\Tripletex\Tripletex $app, $line_item, array $context = array(), $send = FALSE) {

  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  $tripletex_order_line = new \zaporylie\Tripletex\Model\Order\OrderLine();
  $tripletex_order_line->setCount($line_item_wrapper->quantity->value());

  // If Order Line was already created in Tripletex, use reference.
  if (!empty($line_item->data['commerce_tripletex']['remote_id'])) {
    try {
      $app->orderLine()->getOrderLine($line_item->data['commerce_tripletex']['remote_id']);
      $tripletex_order_line->setId($line_item->data['commerce_tripletex']['remote_id']);
    }
    catch (Exception $exception) {
      // Mute.
    }
  }

  // Set order reference.
  if (isset($context['tripletex_order']) && $context['tripletex_order'] instanceof \zaporylie\Tripletex\Model\Order\Order) {
    $tripletex_order_line->setOrder($context['tripletex_order']);
  }

  // Try to use product reference if possible.
  if (in_array($line_item_wrapper->getBundle(), commerce_product_line_item_types())
    && (($product = $line_item_wrapper->commerce_product->value())
      && !empty($product->data['commerce_tripletex']['remote_id']))) {
    $tripletex_order_line->setProduct((new \zaporylie\Tripletex\Model\Product\Product())->setId($product->data['commerce_tripletex']['remote_id']));
  }
  else {
    // Fallback to custom line item.
    $tripletex_order_line->setDescription(commerce_line_item_title($line_item_wrapper->value()));
  }

  // Get VAT.
  if (module_exists('commerce_vat')) {
    // Get calculation direction.
    $direction = variable_get('commerce_vat_direction', 'forward');

    // Get price components.
    $price_data = $line_item_wrapper->commerce_unit_price->data->value();

    // Get vat amount from price components.
    $vat_amount = commerce_vat_total_amount($price_data['components'], $direction == 'reverse', $line_item_wrapper->commerce_unit_price->currency_code->value());

    // Get price without VAT.
    $tripletex_order_line->setUnitPriceExcludingVatCurrency(commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value() - $vat_amount, $line_item_wrapper->commerce_unit_price->currency_code->value()));

    // Get vat components to determine vat rate.
    if ($vat_components = commerce_vat_components($price_data['components'])) {
      // Only one vat rate for line item.
      $vat_component = reset($vat_components);

      // Set only if map found.
      if ($vat_type_id = _commerce_tripletex_match_vat_rate($vat_component)) {
        $tripletex_order_line->setVatType((new \zaporylie\Tripletex\Model\Ledger\VatType())->setId($vat_type_id));
      }
    }
  }
  else {
    throw new \Exception('Only Commerce VAT is implemented at the moment.');
  }

  drupal_alter('commerce_tripletex_sync_order_line', $app, $tripletex_order_line, $line_item, $context);

  // Send to Tripletex.
  if ($send && !$tripletex_order_line->getId()) {
    $tripletex_order_line = $app->orderLine()
      ->createOrderLine($tripletex_order_line)
      ->getValue();
  }
  elseif ($send) {
    throw new Exception('Order line update is currently unavailable');
  }

  return $tripletex_order_line;
}

/**
 * Updates commerce_product from (!) Tripletex
 *
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param object $product
 *
 * @return \zaporylie\Tripletex\Model\Product\Product
 *
 * @throws
 */
function commerce_tripletex_sync_product(\zaporylie\Tripletex\Tripletex $app, $product) {

  if (empty($product->data['commerce_tripletex']['remote_id'])) {
    throw new Exception('Missing remote id');
  }

  // Get product from Tripletex.
  $tripletex_product = $app->product()
    ->getProduct($product->data['commerce_tripletex']['remote_id'])
    ->getValue();

  // Sync standard properties.
  $product->title = $tripletex_product->getName();
  $product->sku = $tripletex_product->getNumber();

  // Allow syncing custom properties.
  drupal_alter('commerce_tripletex_sync_product', $app, $product, $tripletex_product);

  return $tripletex_product;
}

/**
 * Creates (or updates) consumer in Tripletex.
 *
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param object $user
 * @param array $context
 *
 * @return \zaporylie\Tripletex\Model\Customer\Customer
 */
function commerce_tripletex_sync_user(\zaporylie\Tripletex\Tripletex $app, $user, array $context = array()) {

  // Create customer.
  $tripletex_customer = new \zaporylie\Tripletex\Model\Customer\Customer();
  $tripletex_customer->setEmail($user->mail);
  $tripletex_customer->setInvoiceEmail($user->mail);
  $tripletex_customer->setIsCustomer(TRUE);

  if (isset($context['billing_address']) && $context['billing_address'] instanceof \zaporylie\Tripletex\Model\Address\Address) {
    /** @var \zaporylie\Tripletex\Model\Address\Address $billing_address */
    $billing_address = $context['billing_address'];
    $tripletex_customer->setPhysicalAddress($billing_address);
    $tripletex_customer->setPostalAddress($billing_address);
  }

  // Name is required by external API.
  if (!isset($context['name'])) {
    throw new Exception('Missing name');
  }
  $tripletex_customer->setName($context['name']);

  if (isset($context['shipping_address']) && $context['shipping_address'] instanceof \zaporylie\Tripletex\Model\Address\Address) {
    /** @var \zaporylie\Tripletex\Model\Address\Address $shipping_address */
    $shipping_address = $context['shipping_address'];
    $tripletex_customer->setDeliveryAddress($shipping_address);
  }

  // If Order was already created in Tripletex, use reeference.
  if (!empty($user->data['commerce_tripletex']['remote_id'])) {
    try {
      $app->customer()->getCustomer($user->data['commerce_tripletex']['remote_id']);
      // If no exception thrown, use remote id.
      $tripletex_customer->setId($user->data['commerce_tripletex']['remote_id']);
    }
    catch (Exception $exception) {
      // Mute.
    }
  }

  // Allow to customize Customer before sending it to Tripletex.
  drupal_alter('commerce_tripletex_sync_user', $app, $tripletex_customer, $user);

  // Create or update Customer in Tripletex.
  if ($tripletex_customer->getId()) {
    $tripletex_customer = $app->customer()->updateCustomer($tripletex_customer)->getValue();
  }
  else {
    $tripletex_customer = $app->customer()->createCustomer($tripletex_customer)->getValue();
    // Set user.
    $user->data['commerce_tripletex']['remote_id'] = $tripletex_customer->getId();
  }

  return $tripletex_customer;
}

/**
 * Creates (or updates) profile in Tripletex.
 *
 * @param \zaporylie\Tripletex\Tripletex $app
 * @param object $profile
 */
function commerce_tripletex_sync_profile(\zaporylie\Tripletex\Tripletex $app, $profile, $save = FALSE) {
  $wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile);

  $address = (new \zaporylie\Tripletex\Model\Address\Address())
    ->setAddressLine1($wrapper->commerce_customer_address->thoroughfare->value())
    ->setAddressLine2($wrapper->commerce_customer_address->premise->value())
    ->setCity($wrapper->commerce_customer_address->locality->value())
    ->setPostalCode($wrapper->commerce_customer_address->postal_code->value());

  drupal_alter('commerce_tripletex_sync_profile', $app, $address, $profile);

  return $address;
}

/**
 * Match price component to Tripletex Vat Type.
 *
 * @param $vat_component
 *
 * @return false|int|string
 */
function _commerce_tripletex_match_vat_rate($vat_component) {
  // Get vat rate.
  $vat_rate = $vat_component['price']['data']['vat_rate']['price_component'];
  $vat_rate = str_replace('vat|', '', $vat_rate);

  // Get vat map.
  $map = variable_get('commerce_tripletex_vat_map', array());
  $map = array_filter($map);

  // Return correct vat type id.
  return array_search($vat_rate, $map, TRUE);
}

/**
 * Get list of VAT types from Tripletex.
 *
 * Results are cached locally to ensure basic performance.
 *
 * @return \zaporylie\Tripletex\Model\Ledger\ResponseVatTypeList
 */
function _commerce_tripletex_vat_rates($reset = FALSE) {
  $cache = cache_get('commerce_tripletex_vat_rates');
  if (empty($cache->data) || !($cache->data instanceof \zaporylie\Tripletex\Model\Ledger\ResponseVatTypeList) || $reset) {
    try {
      $app = tripletex_api_get_app(tripletex_api_get_user());
      $cache->data = $app->ledger()->getVatTypeList();
      cache_set('commerce_tripletex_vat_rates', $cache->data);
    }
    catch (Exception $exception) {
      watchdog_exception('commerce_tripletex', $exception);
      return new \zaporylie\Tripletex\Model\Ledger\ResponseVatTypeList();
    }
  }
  return $cache->data;
}
