<?php

/**
 * VAT map form.
 */
function commerce_tripletex_vat_map($form, &$form_state) {

  form_load_include($form_state, 'inc', 'commerce_tripletex');

  $map = variable_get('commerce_tripletex_vat_map', array());
  $map = array_filter($map);

  $form['commerce_tripletex_vat_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vat rate map'),
    '#tree' => TRUE,
  );

  // List all rates.
  foreach (_commerce_tripletex_vat_rates()->getValues() as $vat_rate) {
    $form['commerce_tripletex_vat_map'][$vat_rate->getId()] = array(
      '#type' => 'select',
      '#title' => t('@title [@id]', array('@title' => $vat_rate->getName(), '@id' => $vat_rate->getId())),
      '#default_value' => isset($map[$vat_rate->getId()]) ? $map[$vat_rate->getId()] : NULL,
      '#options' => array(NULL => t('- Choose -')) + commerce_vat_rate_titles(),
    );
  }

  return system_settings_form($form);
}

/**
 * Order sync page.
 */
function commerce_tripletex_order_page($order) {
  $build = array();

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_user = $wrapper->owner->value();

  $app = tripletex_api_get_app(tripletex_api_get_user());
  try {
    if (empty($order_user->data['commerce_tripletex']['remote_id'])) {
      throw new Exception('Missing remote ID for customer');
    }
    $tripletex_customer = $app->customer()->getCustomer($order_user->data['commerce_tripletex']['remote_id'])->getValue();
    $customer_remote = format_string('@customer_number [id: @customer_id]', array('@customer_number' => $tripletex_customer->getCustomerNumber(), '@customer_id' => $tripletex_customer->getId()));
  }
  catch (\zaporylie\Tripletex\Exception\ApiException $e) {
    drupal_set_message(t('Tripletex Error: @message', array('@message' => $e->getMessage())), 'error');
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'warning');
  }
  $build['customer'] = array(
    '#type' => 'item',
    '#title' => t('Customer'),
    '#markup' => isset($customer_remote) ? $customer_remote : t('Not synchronized'),
  );
  if (isset($tripletex_customer)) {
    $build['customer_data'] = array(
      '#type' => 'fieldset',
      '#title' => t('Data'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'data' => array(
        '#markup' => '<pre>' . print_r($tripletex_customer, TRUE) . '</pre>',
      ),
    );
  }

  try {
    if (empty($order->data['commerce_tripletex']['remote_id'])) {
      throw new Exception('Missing remote ID for order');
    }
    $tripletex_order = $app->order()->getOrder($order->data['commerce_tripletex']['remote_id'])->getValue();
    $order_remote = format_string('@order_number [id: @order_id]', array('@order_number' => $tripletex_order->getNumber(), '@order_id' => $tripletex_order->getId()));
  }
  catch (\zaporylie\Tripletex\Exception\ApiException $e) {
    drupal_set_message(t('Tripletex Error: @message', array('@message' => $e->getMessage())), 'error');
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'warning');
  }
  $build['order'] = [
    '#type' => 'item',
    '#title' => t('Order'),
    '#markup' => isset($order_remote) ? $order_remote : t('Not synchronized'),
  ];
  if (isset($tripletex_order)) {
    $build['order_data'] = array(
      '#type' => 'fieldset',
      '#title' => t('Data'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'data' => array(
        '#markup' => '<pre>' . print_r($tripletex_order, TRUE) . '</pre>',
      ),
    );
  }

  $build['form'] = drupal_get_form('commerce_tripletex_synchronize_order_form', $order);

  return $build;
}

/**
 * Sync order form - form handler.
 */
function commerce_tripletex_synchronize_order_form($form, &$form_state, $order) {

  $form_state['#entity'] = $order;
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Synchronize'),
  );

  return $form;
}

/**
 * Sync order form - submit handler.
 */
function commerce_tripletex_synchronize_order_form_submit($form, &$form_state) {

  // Load helpers.
  module_load_include('inc', 'commerce_tripletex');

  // Re-load order.
  $order = commerce_order_load($form_state['#entity']->order_id);

  try {
    // Get user to be used with API.
    $user = tripletex_api_get_user();

    // Load order wrapper for easy access to sub-entities.
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // Load user - order's owner.
    $order_user = $wrapper->owner->value();
    if (!$order_user) {
      throw new Exception('Missing user');
    }

    // Get an app.
    $app = tripletex_api_get_app($user);

    if (!$wrapper->__isset('commerce_customer_billing') || !$wrapper->commerce_customer_billing->__isset('commerce_customer_address')) {
      throw new Exception('Missing billing address');
    }

    // Create billing address.
    $billing_address = commerce_tripletex_sync_profile($app, $wrapper->commerce_customer_billing->value());


    if (!$wrapper->__isset('commerce_customer_shipping') || !$wrapper->commerce_customer_shipping->__isset('commerce_customer_address')) {
      throw new Exception('Missing shipping address');
    }

    // Create shipping address.
    $shipping_address = commerce_tripletex_sync_profile($app, $wrapper->commerce_customer_shipping->value());

    // Save customer.
    $tripletex_customer = commerce_tripletex_sync_user($app, $order_user, array(
      'billing_address' => $billing_address,
      'shipping_address' => $shipping_address,
      'name' => $wrapper->commerce_customer_billing->commerce_customer_address->name_line->value(),
    ));

    // @todo: Resave only if needed.
    user_save($order_user);

    $tripletex_order = commerce_tripletex_sync_order($app, $order, array(
      'tripletex_customer' => $tripletex_customer,
    ));
    commerce_order_save($order);

    /** @var \zaporylie\Tripletex\Model\Order\OrderLine[] */
    $tripletex_order_lines = array();
    foreach ($wrapper->commerce_line_items->getIterator() as $line_item_wrapper) {
      $tripletex_order_line = commerce_tripletex_sync_order_line($app, $line_item_wrapper->value(), array(
        'tripletex_order' => $tripletex_order,
      ),
      TRUE);

      // Save line item reference.
      // @todo: Save only if new.
      $line_item = $line_item_wrapper->value();
      $line_item->data['commerce_tripletex']['remote_id'] = $tripletex_order_line->getId();
      $line_item_wrapper->save();
    }
//
//    // Save order lines (all at once).
//    $tripletex_order_lines = $app->orderLine()->createOrderLines(array_values($tripletex_order_lines))->getValues();
//
//    // Save line reference order line.
//    foreach ($wrapper->commerce_line_items->getIterator() as $line_item_wrapper) {
//      // If for some reason item doesn't exist in Tripletex - skip.
//      if (!isset($tripletex_order_lines[$line_item_wrapper->getIdentifier()])) {
//        continue;
//      }
//      /** @var \zaporylie\Tripletex\Model\Order\OrderLine $tripletex_order_line */
//      $tripletex_order_line = $tripletex_order_lines[$line_item_wrapper->getIterator()];
//
//      // Save line item reference.
//      $line_item = $line_item_wrapper->value();
//      $line_item->data['commerce_tripletex']['remote_id'] = $tripletex_order_line->getId();
//      $line_item_wrapper->save();
//    }

    drupal_set_message(t('Order synchronized with Tripletex'));
  }
  catch (Exception $e) {
    watchdog('commerce_tripletex', 'Order @order_id could not be synchronized: %message', array('@order_id' => $order->order_id, '%message' => $e->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Order sync could not be finalized due to fatal error: %message', array('%message' => $e->getMessage())), 'error');
    $form_state['rebuild'] = TRUE;
  }

}

/**
 * Tripletex - product page.
 */
function commerce_tripletex_product_page($product) {
  $build = array();

  $build['form'] = drupal_get_form('commerce_tripletex_synchronize_product_form', $product);

  if (!empty($product->data['commerce_tripletex']['remote_id'])) {
    try {
      $user = tripletex_api_get_user();
      $app = tripletex_api_get_app($user);

      $tripletex_product = $app->product()
        ->getProduct($product->data['commerce_tripletex']['remote_id'])
        ->getValue();

      $build['data'] = [
        '#type' => 'fieldset',
        '#title' => t('Data'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        'data' => [
          '#markup' => '<pre>' . print_r($tripletex_product, TRUE) . '</pre>',
        ],
      ];
    }
    catch (Exception $exception) {
      watchdog_exception('commerce_tripletex', $exception);
    }
  }

  return $build;
}

/**
 * Product sync form handler.
 */
function commerce_tripletex_synchronize_product_form($form, &$form_state, $product) {

  $form_state['#entity'] = $product;

  $form['remote_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote ID'),
    '#required' => TRUE,
    '#default_value' => isset($product->data['commerce_tripletex']['remote_id']) ? $product->data['commerce_tripletex']['remote_id'] : NULL,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save & sync'),
  );

  return $form;
}

/**
 * Product sync form submit handler.
 */
function commerce_tripletex_synchronize_product_form_submit($form, $form_state) {
  module_load_include('inc', 'commerce_tripletex');

  // Reload product.
  $product = commerce_product_load($form_state['#entity']->product_id);

  // Save remote id on product.
  if (empty($product->data['commerce_tripletex']['remote_id']) || ($product->data['commerce_tripletex']['remote_id'] != $form_state['values']['remote_id'])) {
    $product->data['commerce_tripletex']['remote_id'] = $form_state['values']['remote_id'];
    commerce_product_save($product);
  }

  try {
    // Get user and app.
    $user = tripletex_api_get_user();
    $app = tripletex_api_get_app($user);

    // Sync product.
    commerce_tripletex_sync_product($app, $product);

    // Save product.
    commerce_product_save($product);

    // Log.
    watchdog('commerce_tripletex', 'Product @sku synced from Tripletex.', array('@sku' => $product->sku), WATCHDOG_INFO);

    // Set message.
    drupal_set_message(t('Product synced.'));
  }
  catch (Exception $exception) {
    drupal_set_message(t('Unable to sync product: @message', array('@message' => $exception->getMessage())), 'error');
    $form_state['rebuild'] = TRUE;
  }
}



