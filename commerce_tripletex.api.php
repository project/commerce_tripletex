<?php

/**
 * Allows commerce product alter based on data from Tripletex.
 *
 * @param object $product
 *   Commerce product object.
 * @param \zaporylie\Tripletex\Model\Product\Product $tripletex_product
 *   Tripletex product fetched, based on remote id, from Tripletex.
 */
function hook_commerce_tripletex_sync_product_alter($product, \zaporylie\Tripletex\Model\Product\Product $tripletex_product) {

}


